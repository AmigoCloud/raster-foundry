/* eslint max-len: 0 */
import _ from 'lodash';

import rootTpl from './pages/root/root.html';
import loginTpl from './pages/login/login.html';

import labBrowseTpl from './pages/lab/browse/browse.html';
import labBrowseModule from './pages/lab/browse/browse';
import labBrowseAnalysesTpl from './pages/lab/browse/analyses/analyses.html';
import labBrowseTemplatesTpl from './pages/lab/browse/templates/templates.html';
import labTemplateTpl from './pages/lab/template/template.html';
import labAnalysisTpl from './pages/lab/analysis/analysis.html';
import labAnalysisModule from './pages/lab/analysis/analysis';
import labStartAnalysisTpl from './pages/lab/startAnalysis/startAnalysis.html';
import labNavbarTpl from './pages/lab/navbar/navbar.html';

import projectsModule from './pages/projects/projects.module';
import projectsTpl from './pages/projects/projects.html';
import projectsNavbarTpl from './pages/projects/navbar/navbar.html';
import projectsEditTpl from './pages/projects/edit/edit.html';

import projectsEditColorTpl from './pages/projects/edit/color/color.html';
import projectsEditColormodeTpl from './pages/projects/edit/colormode/colormode.html';
import projectsAdvancedColorTpl from './pages/projects/edit/advancedcolor/advancedcolor.html';
import projectsColorAdjustTpl from './pages/projects/edit/advancedcolor/adjust/adjust.html';
import projectsListTpl from './pages/projects/list/list.html';
import projectsScenesTpl from './pages/projects/edit/scenes/scenes.html';
import projectsSceneBrowserTpl from './pages/projects/edit/browse/browse.html';
import projectOrderScenesTpl from './pages/projects/edit/order/order.html';
import projectMaskingTpl from './pages/projects/edit/masking/masking.html';
import projectMaskingDrawTpl from './pages/projects/edit/masking/draw/draw.html';
import aoiApproveTpl from './pages/projects/edit/aoi-approve/aoi-approve.html';
import aoiParametersTpl from './pages/projects/edit/aoi-parameters/aoi-parameters.html';
import exportTpl from './pages/projects/edit/exports/exports.html';
import newExportTpl from './pages/projects/edit/exports/new/new.html';
import annotateTpl from './pages/projects/edit/annotate/annotate.html';
import annotateImportTpl from './pages/projects/edit/annotate/import/import.html';
import annotateExportTpl from './pages/projects/edit/annotate/export/export.html';
import projectSharingTpl from './pages/projects/edit/sharing/sharing.html';

import userTpl from './pages/user/user.html';
import userModule from './pages/user/user';
import userSettingsTpl from './pages/user/settings/settings.html';
import userSettingsProfileTpl from './pages/user/settings/profile/profile.html';
import userSettingsApiTokensTpl from './pages/user/settings/api/api.html';
import userSettingsMapTokensTpl from './pages/user/settings/map/map.html';
import userSettingsConnectionsTpl from './pages/user/settings/connections/connections.html';
import userSettingsPrivacyTpl from './pages/user/settings/privacy/privacy.html';
import userSettingsNotificationTpl from './pages/user/settings/notification/notification.html';
import userOrganizationsTpl from './pages/user/organizations/organizations.html';
import userTeamsTpl from './pages/user/teams/teams.html';
import userProjectsTpl from './pages/user/projects/projects.html';
import userRastersTpl from './pages/user/rasters/rasters.html';

import errorTpl from './pages/error/error.html';
import shareTpl from './pages/share/share.html';
import homeTpl from './pages/home/home.html';
import importsTpl from './pages/imports/imports.html';
import importsModule from './pages/imports/imports.module';
import rasterListTpl from './pages/imports/raster/raster.html';
import vectorListTpl from './pages/imports/vector/vector.html';
import importsDatasourcesTpl from './pages/imports/datasources/datasources.html';
import importsDatasourcesListTpl from './pages/imports/datasources/list/list.html';
import importsDatasourcesDetailTpl from './pages/imports/datasources/detail/detail.html';

import adminTpl from './pages/admin/admin.html';
import organizationTpl from './pages/admin/organization/organization.html';
import organizationModule from './pages/admin/organization/organization';
import organizationMetricsTpl from './pages/admin/organization/metrics/metrics.html';
import organizationUsersTpl from './pages/admin/organization/users/users.html';
import organizationTeamsTpl from './pages/admin/organization/teams/teams.html';
import organizationSettingsTpl from './pages/admin/organization/settings/settings.html';
import organizationProjectsTpl from './pages/admin/organization/projects/projects.html';
import organizationRastersTpl from './pages/admin/organization/rasters/rasters.html';
import organizationVectorsTpl from './pages/admin/organization/vectors/vectors.html';
import organizationDatasourcesTpl from './pages/admin/organization/datasources/datasources.html';
import organizationTemplatesTpl from './pages/admin/organization/templates/templates.html';
import organizationAnalysesTpl from './pages/admin/organization/analyses/analyses.html';
import platformTpl from './pages/admin/platform/platform.html';
import platformModule from './pages/admin/platform/platform.module';
import platformMetricsTpl from './pages/admin/platform/metrics/metrics.html';
import platformUsersTpl from './pages/admin/platform/users/users.html';
import platformOrganizationsTpl from './pages/admin/platform/organizations/organizations.html';
import platformSettingsTpl from './pages/admin/platform/settings/settings.html';
import platformSettingsEmailTpl from './pages/admin/platform/settings/email/email.html';
import platformProjectsTpl from './pages/admin/platform/projects/projects.html';
import platformRastersTpl from './pages/admin/platform/rasters/rasters.html';

import platformVectorsTpl from './pages/admin/platform/vectors/vectors.html';
import platformDatasourcesTpl from './pages/admin/platform/datasources/datasources.html';
import platformTemplatesTpl from './pages/admin/platform/templates/templates.html';
import platformAnalysesTpl from './pages/admin/platform/analyses/analyses.html';
import teamTpl from './pages/admin/team/team.html';
import teamModule from './pages/admin/team/team';
import teamUsersTpl from './pages/admin/team/users/users.html';
import teamProjectsTpl from './pages/admin/team/projects/projects.html';
import teamRastersTpl from './pages/admin/team/rasters/rasters.html';
import teamVectorsTpl from './pages/admin/team/vectors/vectors.html';
import teamDatasourcesTpl from './pages/admin/team/datasources/datasources.html';
import teamTemplatesTpl from './pages/admin/team/templates/templates.html';
import teamAnalysesTpl from './pages/admin/team/analyses/analyses.html';

import { projectResolves } from './components/pages/project';


function shareStatesV2($stateProvider) {
    $stateProvider
        .state('/v2/share/?mapToken')
        .state('/v2/share/project/:projectId?mapToken')
        .state('/v2/share/layer/:layerId?mapToken')
    ;
}

function projectStatesV2($stateProvider) {
    let addScenesQueryParams = [
        'maxCloudCover',
        'minCloudCover',
        'minAcquisitionDatetime',
        'maxAcquisitionDatetime',
        'datasource',
        'maxSunAzimuth',
        'minSunAzimuth',
        'maxSunElevation',
        'minSunElevation',
        'bbox',
        'point',
        'ingested',
        'owner'
    ].join('&');

    $stateProvider
        .state('project', {
            parent: 'root',
            title: 'Project',
            url: '/v2/project/:projectId',
            resolve: Object.assign({
                projectId: ['$transition$', ($transition$) => $transition$.params().projectId],
                project: [
                    '$transition$', 'projectService',
                    ($transition$, projectService) =>
                        projectService.fetchProject($transition$.params().projectId)
                ]
            }, projectResolves.resolve),
            redirectTo: 'project.layers',
            views: {
                'projectlayernav': {
                    component: 'rfProjectLayersNav'
                },
                '': {
                    component: 'rfProjectPage'
                }
            }
        })
        .state('project.layers', {
            title: 'Project Layers',
            url: '/layers?page',
            params: {
                page: { dynamic: true }
            },
            component: 'rfProjectLayersPage'
        })
    // top level project routes
        .state('project.analyses', {
            title: 'Project Analyses',
            url: '/analyses?page&search',
            component: 'rfProjectAnalysesPage'
        })
        .state('project.settings', {
            title: 'Project Settings',
            url: '/settings',
            redirectTo: 'project.settings.options',
            views: {
                'projectlayernav@root': {
                    component: 'rfProjectLayersNav'
                },
                '': {
                    component: 'rfProjectSettingsPage'
                }
            }
        })
        .state('project.layer', {
            title: 'Project Layer',
            url: '/layer/:layerId',
            resolve: {
                layerId: ['$transition$', ($transition$) => $transition$.params().layerId],
                layer: [
                    '$transition$', 'projectService',
                    ($transition$, projectService) =>
                        projectService.getProjectLayer(
                            $transition$.params().projectId,
                            $transition$.params().layerId
                        )
                ]
            },
            views: {
                'projectlayernav@root': {
                    component: 'rfProjectLayersNav'
                },
                '': {
                    component: 'rfProjectLayerPage'
                }
            }
        })
    // project layer routes
        .state('project.layer.aoi', {
            title: 'Project Layer AOI',
            url: '/aoi',
            component: 'rfProjectLayerAoiPage'
        })
        .state('project.layer.colormode', {
            title: 'Project Layer Colormode',
            url: '/colormode',
            component: 'rfProjectLayerColormodePage'
        })
        .state('project.layer.corrections', {
            title: 'Project Layer Corrections',
            url: '/corrections',
            component: 'rfProjectLayerCorrectionsPage'
        })
        .state('project.layer.scenes', {
            title: 'Project Layer Scenes',
            url: '/scenes?page',
            component: 'rfProjectLayerScenesPage'
        })
        .state('project.layer.scenes.browse', {
            title: 'Find Scenes',
            url: '/browse?' + addScenesQueryParams,
            component: 'rfProjectLayerScenesBrowsePage'
        })
        .state('project.layer.exports', {
            title: 'Project Layer Exports',
            url: '/exports?page&search',
            component: 'rfProjectLayerExportsPage'
        })
        .state('project.layer.export', {
            title: 'Project Layer Export',
            url: '/export',
            component: 'rfProjectLayerExportPage'
        })
        .state('project.layer.annotations', {
            title: 'Project Layer Annotations',
            url: '/annotations?page',
            component: 'rfProjectLayerAnnotationsPage'
        })
        .state('project.layer.annotate', {
            title: 'Project Layer Annotate',
            url: '/annotate',
            component: 'rfProjectLayerAnnotatePage'
        })
    // Project analyses routes
        .state('project.analyses.compare', {
            title: 'Compare Project Analyses',
            url: '/compare?id',
            component: 'rfProjectAnalysesComparePage'
        })
        .state('project.analyses.settings', {
            title: 'Project Analyses Settings',
            url: '/settings',
            component: 'rfProjectAnalysesSettingsPage',
            redirectTo: 'project.analyses.settings.options'
        })
        .state('project.analyses.settings.options', {
            title: 'Project Analyses Options',
            url: '/options',
            component: 'rfProjectAnalysesOptionsPage'
        })
        .state('project.analyses.settings.masking', {
            title: 'Project Analyses Masking',
            url: '/masking',
            component: 'rfProjectAnalysesMaskingPage'
        })
        .state('project.analyses.settings.publishing', {
            title: 'Project Analyses Publishing',
            url: '/publishing',
            component: 'rfProjectAnalysesPublishingPage'
        })
        .state('project.analyses.settings.permissions', {
            title: 'Project Analyses Permissions',
            url: '/permissions',
            component: 'rfProjectAnalysesPermissionsPage'
        })
        .state('project.analyses.visualize', {
            title: 'Project Analyses Visualization',
            url: '/visualize',
            component: 'rfProjectAnalysesVisualizePage'
        })
    // project settings routes
        .state('project.settings.options', {
            title: 'Project Options',
            url: '/options',
            component: 'rfProjectOptionsPage'
        })
        .state('project.settings.publishing', {
            title: 'Project Publishing',
            url: '/publishing',
            component: 'rfProjectPublishingPage'
        })
        .state('project.settings.permissions', {
            title: 'Project Permissions',
            url: '/permissions',
            component: 'rfProjectPermissionsPage'
        });
}

function projectEditStates($stateProvider) {
    let addScenesQueryParams = [
        'maxCloudCover',
        'minCloudCover',
        'minAcquisitionDatetime',
        'maxAcquisitionDatetime',
        'datasource',
        'maxSunAzimuth',
        'minSunAzimuth',
        'maxSunElevation',
        'minSunElevation',
        'bbox',
        'point',
        'ingested',
        'owner'
    ].join('&');

    $stateProvider
        .state('projects.edit', {
            title: 'Proyecto: Editar',
            url: '/edit/:projectid',
            params: {project: null},
            views: {
                'navmenu@root': {
                    templateUrl: projectsNavbarTpl,
                    controller: 'ProjectsNavbarController',
                    controllerAs: '$ctrl'
                },
                '': {
                    templateUrl: projectsEditTpl,
                    controller: 'ProjectsEditController',
                    controllerAs: '$ctrl'
                }
            },
            redirectTo: 'projects.edit.scenes'
        })
        .state('projects.edit.colormode', {
            title: 'Proyecto: Color',
            url: '/colormode',
            templateUrl: projectsEditColormodeTpl,
            controller: 'ProjectsEditColormodeController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.color', {
            title: 'Proyecto: Corrección de color',
            url: '/color',
            templateUrl: projectsEditColorTpl,
            controller: 'ProjectsEditColorController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.advancedcolor', {
            title: 'Proyecto: Corrección de color',
            url: '/advancedcolor',
            templateUrl: projectsAdvancedColorTpl,
            controller: 'ProjectsAdvancedColorController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.advancedcolor.adjust', {
            title: 'Proyecto: Corrección de color',
            url: '/adjust',
            templateUrl: projectsColorAdjustTpl,
            controller: 'ProjectsColorAdjustController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.scenes', {
            title: 'Proyecto: Escenas',
            url: '/scenes?page',
            templateUrl: projectsScenesTpl,
            controller: 'ProjectsScenesController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.browse', {
            title: 'Proyecto: Escenas',
            url: '/browse/?sceneid' + addScenesQueryParams,
            templateUrl: projectsSceneBrowserTpl,
            controller: 'ProjectsSceneBrowserController',
            controllerAs: '$ctrl',
            reloadOnSearch: false
        })
        .state('projects.edit.order', {
            url: '/order',
            templateUrl: projectOrderScenesTpl,
            controller: 'ProjectsOrderScenesController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.masking', {
            url: '/masking',
            templateUrl: projectMaskingTpl,
            controller: 'ProjectsMaskingController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.masking.draw', {
            url: '/mask',
            templateUrl: projectMaskingDrawTpl,
            controller: 'ProjectsMaskingDrawController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.aoi-approve', {
            title: 'Proyecto: Escenas en espera',
            url: '/aoi-approve',
            templateUrl: aoiApproveTpl,
            controller: 'AOIApproveController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.aoi-parameters', {
            title: 'Proyecto: Área de interés',
            url: '/aoi-parameters',
            templateUrl: aoiParametersTpl,
            controller: 'AOIParametersController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.exports', {
            title: 'Proyecto: Exportaciones',
            url: '/exports',
            templateUrl: exportTpl,
            controller: 'ExportController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.exports.new', {
            title: 'Proyecto: Nueva exportación',
            url: '/new',
            templateUrl: newExportTpl,
            controller: 'NewExportController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.annotate', {
            title: 'Proyecto: Anotaciones',
            url: '/annotate',
            templateUrl: annotateTpl,
            controller: 'AnnotateController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.annotate.import', {
            url: '/import',
            templateUrl: annotateImportTpl,
            controller: 'AnnotateImportController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.annotate.export', {
            url: '/export',
            templateUrl: annotateExportTpl,
            controller: 'AnnotateExportController',
            controllerAs: '$ctrl'
        })
        .state('projects.edit.sharing', {
            url: '/sharing',
            templateUrl: projectSharingTpl,
            controller: 'SharingController',
            controllerAs: '$ctrl'
        });
}

function projectStates($stateProvider) {
    $stateProvider
        .state('projects', {
            parent: 'root',
            url: '/projects',
            templateUrl: projectsTpl,
            controller: 'ProjectsController',
            controllerAs: '$ctrl',
            abstract: true,
            resolve: projectsModule.resolve
        })
        .state('projects.list', {
            title: 'Proyectos',
            url: '/list?page&search&ownership',
            templateUrl: projectsListTpl,
            controller: 'ProjectsListController',
            controllerAs: '$ctrl'
        });
    projectEditStates($stateProvider);
}

function settingsStates($stateProvider) {
    $stateProvider
        .state('user', {
            parent: 'root',
            url: '/user/:userId',
            templateUrl: userTpl,
            controller: 'UserController',
            controllerAs: '$ctrl',
            redirectTo: 'user.projects',
            resolve: userModule.resolve
        })
        .state('user.settings', {
            url: '/settings',
            templateUrl: userSettingsTpl,
            controller: 'SettingsController',
            controllerAs: '$ctrl',
            redirectTo: 'user.settings.profile'
        })
        .state('user.settings.profile', {
            title: 'Ajustes de perfil',
            url: '/profile',
            templateUrl: userSettingsProfileTpl,
            controller: 'ProfileController',
            controllerAs: '$ctrl'
        })
        .state('user.settings.api-tokens', {
            title: 'Ajustes: Tokens de la API',
            url: '/api-tokens?code&state',
            templateUrl: userSettingsApiTokensTpl,
            controller: 'ApiTokensController',
            controllerAs: '$ctrl'
        })
        .state('user.settings.map-tokens', {
            title: 'Ajustes: tokens de mapas',
            url: '/map-tokens',
            templateUrl: userSettingsMapTokensTpl,
            controller: 'MapTokensController',
            controllerAs: '$ctrl'
        })
        .state('user.settings.connections', {
            title: 'Ajustes: Conexiones a la API',
            url: '/connections',
            templateUrl: userSettingsConnectionsTpl,
            controller: 'ConnectionsController',
            controllerAs: '$ctrl'
        })
        .state('user.settings.privacy', {
            title: 'Ajustes: Privacidad',
            url: '/privacy',
            templateUrl: userSettingsPrivacyTpl,
            controller: 'PrivacyController',
            controllerAs: '$ctrl'
        })
        .state('user.settings.notification', {
            title: 'Ajustes: Notificaciones',
            url: '/notification',
            templateUrl: userSettingsNotificationTpl,
            controller: 'NotificationController',
            controllerAs: '$ctrl'
        })
        .state('user.organizations', {
            title: 'Organizaciones',
            url: '/organizations?page',
            templateUrl: userOrganizationsTpl,
            controller: 'UserOrganizationsController',
            controllerAs: '$ctrl'
        })
        .state('user.teams', {
            title: 'Equipos',
            url: '/teams?page',
            templateUrl: userTeamsTpl,
            controller: 'UserTeamsController',
            controllerAs: '$ctrl'
        })
        .state('user.projects', {
            title: 'Proyectos',
            url: '/projects?page&search',
            templateUrl: userProjectsTpl,
            controller: 'UserProjectsController',
            controllerAs: '$ctrl'
        })
        .state('user.rasters', {
            title: 'Rasters',
            url: '/rasters?page',
            templateUrl: userRastersTpl,
            controller: 'UserRastersController',
            controllerAs: '$ctrl'
        });
}

function labStates($stateProvider) {
    $stateProvider
        .state('lab', {
            title: 'Análisis',
            template: '<ui-view></ui-view>',
            parent: 'root',
            url: '/lab',
            redirectTo: 'lab.browse'
        })
        // later on we'll use this to view / edit user templates
        .state('lab.template', {
            title: 'Ver una plantilla',
            url: '/template/:templateid',
            templateUrl: labTemplateTpl,
            controller: 'LabTemplateController',
            controllerAs: '$ctrl'
        })
        .state('lab.startAnalysis', {
            title: 'Iniciar un análisis',
            url: '/start-analysis/:templateid',
            templateUrl: labStartAnalysisTpl,
            controller: 'LabStartAnalysisController',
            controllerAs: '$ctrl'
        })
        .state('lab.analysis', {
            title: 'Detalles del análisis',
            url: '/analysis/:analysisid',
            views: {
                'navmenu@root': {
                    templateUrl: labNavbarTpl,
                    controller: 'LabNavbarController',
                    controllerAs: '$ctrl'
                },
                '': {
                    templateUrl: labAnalysisTpl,
                    controller: 'LabAnalysisController',
                    controllerAs: '$ctrl'
                }
            },
            resolve: labAnalysisModule.resolve
        })
        .state('lab.browse', {
            url: '/browse',
            templateUrl: labBrowseTpl,
            controller: 'LabBrowseController',
            controllerAs: '$ctrl',
            redirectTo: 'lab.browse.analyses',
            resolve: labBrowseModule.resolve
        })
        .state('lab.browse.templates', {
            title: 'Búsqueda de análisis',
            url: '/templates?page&search&query&analysiscategory&analysistag',
            templateUrl: labBrowseTemplatesTpl,
            controller: 'LabBrowseTemplatesController',
            controllerAs: '$ctrl'
        })
        .state('lab.browse.analyses', {
            title: 'Análisis',
            url: '/analyses?page&search&sort',
            templateUrl: labBrowseAnalysesTpl,
            controller: 'LabBrowseAnalysesController',
            controllerAs: '$ctrl'
        });
}

function shareStates($stateProvider) {
    $stateProvider
        .state('share', {
            title: 'Proyecto compartido',
            url: '/share/:projectid',
            templateUrl: shareTpl,
            controller: 'ShareController',
            controllerAs: '$ctrl',
            bypassAuth: true
        });
}

function loginStates($stateProvider) {
    $stateProvider
        .state('login', {
            title: 'Iniciar sesión',
            url: '/login',
            templateUrl: loginTpl,
            controller: 'LoginController',
            controllerAs: '$ctrl',
            bypassAuth: true
        });
}

function homeStates($stateProvider) {
    $stateProvider
        .state('home', {
            parent: 'root',
            url: '/home',
            templateUrl: homeTpl,
            controller: 'HomeController',
            controllerAs: '$ctrl'
        });
}

function importStates($stateProvider) {
    $stateProvider
        .state('imports', {
            title: 'Importaciones',
            parent: 'root',
            url: '/imports',
            templateUrl: importsTpl,
            controller: 'ImportsController',
            controllerAs: '$ctrl',
            abstract: true,
            resolve: importsModule.resolve
        })
        .state('imports.rasters', {
            title: 'Rasters',
            url: '/rasters?page&ownership',
            templateUrl: rasterListTpl,
            controller: 'RasterListController',
            controllerAs: '$ctrl'
        })
        .state('imports.vectors', {
            title: 'Geometrías',
            url: '/vectors?page&search',
            templateUrl: vectorListTpl,
            controller: 'VectorListController',
            controllerAs: '$ctrl'
        })
        .state('imports.datasources', {
            url: '/datasources',
            templateUrl: importsDatasourcesTpl,
            controller: 'DatasourcesController',
            controllerAs: '$ctrl',
            abstract: true
        })
        .state('imports.datasources.list', {
            title: 'Fuentes de datos',
            url: '/list?page&search',
            templateUrl: importsDatasourcesListTpl,
            controller: 'DatasourceListController',
            controllerAs: '$ctrl'
        })
        .state('imports.datasources.detail', {
            title: 'Detalles de fuentes de datos',
            url: '/detail/:datasourceid',
            templateUrl: importsDatasourcesDetailTpl,
            controller: 'DatasourceDetailController',
            controllerAs: '$ctrl'
        });
}

function adminStates($stateProvider) {
    $stateProvider
        .state('admin', {
            parent: 'root',
            title: 'Admin',
            url: '/admin',
            templateUrl: adminTpl,
            controller: 'AdminController',
            controllerAs: '$ctrl'
        })
        .state('admin.organization', {
            title: 'Organización',
            url: '/organization/:organizationId',
            templateUrl: organizationTpl,
            controller: 'OrganizationController',
            controllerAs: '$ctrl',
            resolve: organizationModule.resolve,
            redirectTo: 'admin.organization.projects'
        })
        .state('admin.organization.metrics', {
            title: 'Estadísticas de la organización',
            url: '/metrics',
            templateUrl: organizationMetricsTpl,
            controller: 'OrganizationMetricsController',
            controllerAs: '$ctrl'
        })
        .state('admin.organization.projects', {
            title: 'Proyectos de la organización',
            url: '/projects?page&search',
            templateUrl: organizationProjectsTpl,
            controller: 'OrganizationProjectsController',
            controllerAs: '$ctrl'
        })
        .state('admin.organization.rasters', {
            title: 'Rásters de la organización',
            url: '/rasters?page',
            templateUrl: organizationRastersTpl,
            controller: 'OrganizationRastersController',
            controllerAs: '$ctrl'
        })
        .state('admin.organization.vectors', {
            title: 'Vectores de la organización',
            url: '/vectors?page&search',
            templateUrl: organizationVectorsTpl,
            controller: 'OrganizationVectorsController',
            controllerAs: '$ctrl'
        })
        .state('admin.organization.datasources', {
            title: 'Fuentes de datos de la organización',
            url: '/datasources?page&search',
            templateUrl: organizationDatasourcesTpl,
            controller: 'OrganizationDatasourcesController',
            controllerAs: '$ctrl'
        })
        .state('admin.organization.templates', {
            title: 'Plantillas de la organización',
            url: '/templates?page&search',
            templateUrl: organizationTemplatesTpl,
            controller: 'OrganizationTemplatesController',
            controllerAs: '$ctrl'
        })
        .state('admin.organization.analyses', {
            title: 'Análisis de la organización',
            url: '/analyses?page&search',
            templateUrl: organizationAnalysesTpl,
            controller: 'OrganizationAnalysesController',
            controllerAs: '$ctrl'
        })
        .state('admin.organization.users', {
            title: 'Usuarios de la organización',
            url: '/users?page&search',
            templateUrl: organizationUsersTpl,
            controller: 'OrganizationUsersController',
            controllerAs: '$ctrl'
        })
        .state('admin.organization.teams', {
            title: 'Equipos de la organización',
            url: '/teams?page&search',
            templateUrl: organizationTeamsTpl,
            controller: 'OrganizationTeamsController',
            controllerAs: '$ctrl'
        })
        .state('admin.organization.settings', {
            title: 'Ajustes de la organización',
            url: '/settings',
            templateUrl: organizationSettingsTpl,
            controller: 'OrganizationSettingsController',
            controllerAs: '$ctrl'
        })
        .state('admin.platform', {
            title: 'Plataforma',
            url: '/platform/:platformId',
            templateUrl: platformTpl,
            controller: 'PlatformController',
            controllerAs: '$ctrl',
            resolve: platformModule.resolve,
            redirectTo: 'admin.platform.projects'
        })
        .state('admin.platform.projects', {
            title: 'Proyectos de la plataforma',
            url: '/projects?page&search',
            templateUrl: platformProjectsTpl,
            controller: 'PlatformProjectsController',
            controllerAs: '$ctrl'
        })
        .state('admin.platform.rasters', {
            title: 'Rásters de la plataforma',
            url: '/rasters?page',
            templateUrl: platformRastersTpl,
            controller: 'PlatformRastersController',
            controllerAs: '$ctrl'
        })
        .state('admin.platform.vectors', {
            title: 'Vectores de la plataforma',
            url: '/vectors?page&search',
            templateUrl: platformVectorsTpl,
            controller: 'PlatformVectorsController',
            controllerAs: '$ctrl'
        })
        .state('admin.platform.datasources', {
            title: 'Fuentes de datos de la plataforma',
            url: '/datasources?page&search',
            templateUrl: platformDatasourcesTpl,
            controller: 'PlatformDatasourcesController',
            controllerAs: '$ctrl'
        })
        .state('admin.platform.templates', {
            title: 'Plantillas de la plataforma',
            url: '/templates?page&search',
            templateUrl: platformTemplatesTpl,
            controller: 'PlatformTemplatesController',
            controllerAs: '$ctrl'
        })
        .state('admin.platform.analyses', {
            title: 'Análisis de la plataforma',
            url: '/analyses?page&search',
            templateUrl: platformAnalysesTpl,
            controller: 'PlatformAnalysesController',
            controllerAs: '$ctrl'
        })
        .state('admin.platform.metrics', {
            title: 'Estadísticas de la plataforma',
            url: '/metrics',
            templateUrl: platformMetricsTpl,
            controller: 'PlatformMetricsController',
            controllerAs: '$ctrl'
        })
        .state('admin.platform.users', {
            title: 'Usuarios de la organización',
            url: '/users?page&search',
            templateUrl: platformUsersTpl,
            controller: 'PlatformUsersController',
            controllerAs: '$ctrl'
        })
        .state('admin.platform.settings', {
            url: '/settings',
            title: 'Ajustes de la plataforma',
            templateUrl: platformSettingsTpl,
            controller: 'PlatformSettingsController',
            controllerAs: '$ctrl',
            abstract: true
        })
        .state('admin.platform.settings.email', {
            url: '/email',
            title: 'Ajustes de correo de la plataforma',
            templateUrl: platformSettingsEmailTpl,
            controller: 'PlatformEmailController',
            controllerAs: '$ctrl'
        })
        .state('admin.platform.organizations', {
            title: 'Plataforma: Organizaciones',
            url: '/organizations?page&search',
            templateUrl: platformOrganizationsTpl,
            controller: 'PlatformOrganizationsController',
            controllerAs: '$ctrl'
        })
        .state('admin.team', {
            title: 'Equipo',
            url: '/team/:teamId',
            templateUrl: teamTpl,
            controller: 'AdminTeamController',
            controllerAs: '$ctrl',
            resolve: teamModule.resolve,
            redirectTo: 'admin.team.projects'
        })
        .state('admin.team.projects', {
            title: 'Proyectos del equipo',
            url: '/projects?page&search',
            templateUrl: teamProjectsTpl,
            controller: 'TeamProjectsController',
            controllerAs: '$ctrl'
        })
        .state('admin.team.rasters', {
            title: 'Rásters del equipo',
            url: '/rasters?page',
            templateUrl: teamRastersTpl,
            controller: 'TeamRastersController',
            controllerAs: '$ctrl'
        })
        .state('admin.team.vectors', {
            title: 'Vectores del equipos',
            url: '/vectors?page',
            templateUrl: teamVectorsTpl,
            controller: 'TeamVectorsController',
            controllerAs: '$ctrl'
        })
        .state('admin.team.datasources', {
            title: 'Fuentes de datos del equipo',
            url: '/datasources?page&search',
            templateUrl: teamDatasourcesTpl,
            controller: 'TeamDatasourcesController',
            controllerAs: '$ctrl'
        })
        .state('admin.team.templates', {
            title: 'Plantillas del equipo',
            url: '/templates?page&search',
            templateUrl: teamTemplatesTpl,
            controller: 'TeamTemplatesController',
            controllerAs: '$ctrl'
        })
        .state('admin.team.analyses', {
            title: 'Análisis del equipo',
            url: '/analyses?page&search',
            templateUrl: teamAnalysesTpl,
            controller: 'TeamAnalysesController',
            controllerAs: '$ctrl'
        })
        .state('admin.team.users', {
            url: '/users?page&search',
            title: 'Miembros del equipo',
            templateUrl: teamUsersTpl,
            controller: 'AdminTeamUsersController',
            controllerAs: '$ctrl'
        });
}

function routeConfig(
    $urlRouterProvider, $stateProvider, $urlMatcherFactoryProvider, $locationProvider
) {
    'ngInject';

    $urlMatcherFactoryProvider.strictMode(false);
    $locationProvider.html5Mode(true);


    $stateProvider.state('root', {
        templateUrl: rootTpl,
        controller: 'IndexController'
    }).state('callback', {
        url: '/callback'
    });

    loginStates($stateProvider);
    projectStates($stateProvider);
    projectStatesV2($stateProvider);
    settingsStates($stateProvider);
    labStates($stateProvider);
    shareStates($stateProvider);
    homeStates($stateProvider);
    importStates($stateProvider);
    adminStates($stateProvider);

    $stateProvider
        .state('error', {
            url: '/error',
            templateUrl: errorTpl,
            controller: 'ErrorController',
            controllerAs: '$ctrl',
            bypassAuth: true
        });

    $urlRouterProvider.otherwise('/home');
}


export default angular
    .module('index.routes', [])
    .config(routeConfig);
