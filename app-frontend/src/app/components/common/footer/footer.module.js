import angular from 'angular';
require('../../../../assets/font/icomoon/style.css');

import FooterComponent from './footer.component.js';
import FooterController from './footer.controller.js';

const FooterModule = angular.module('components.common.footer', []);

FooterModule.component('rfFooter', FooterComponent);
FooterModule.controller('FooterController', FooterController);

export default FooterModule;
