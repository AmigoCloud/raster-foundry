/* global BUILDCONFIG, HELPCONFIG, _ */

let assetLogo = require('../../../../assets/images/amigocloud.png');

export default class FooterController {
    constructor( // eslint-disable-line max-params
    ) {
        'ngInject';
    }

    $onInit() {
        this.assetLogo = assetLogo;
    }
}
