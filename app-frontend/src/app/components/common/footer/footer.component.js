// Component code
// https://docs.angularjs.org/guide/component
import footerTpl from './footer.html';

const rfFooter = {
    templateUrl: footerTpl,
    controller: 'FooterController'
};

export default rfFooter;
