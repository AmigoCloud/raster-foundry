/* eslint-disable quotes */
/* global _ */

const statusMapping = {
    "export": {
        "NOTEXPORTED": {
//            "label": "Export in progress",
            "label": "Exportado en proceso",
            "color": "grey"
        },
        "TOBEEXPORTED": {
//            "label": "Queued for export",
            "label": "En cola para exportar",
            "color": "yellow"
        },
        "EXPORTING": {
//            "label": "Currently being exported",
            "label": "Siendo exportado",
            "color": "yellow"
        },
        "EXPORTED": {
//            "label": "Successfully exported",
            "label": "Exportado",
            "color": "green"
        },
        "FAILED": {
//            "label": "Export failed",
            "label": "Falló al exportar",
            "color": "red"
        }
    },
    "import": {
        "CREATED": {
//            "label": "Upload created",
            "label": "Carga creada",
            "color": "grey"
        },
        "UPLOADING": {
//            "label": "Waiting for user to upload files",
            "label": "Esperando que el usuario cargue archivos",
            "color": "yellow"
        },
        "UPLOADED": {
//            "label": "Files uploaded",
            "label": "Archivos cargados",
            "color": "yellow"
        },
        "QUEUED": {
//            "label": "Queued import for processing",
            "label": "Importado en cola para procesar",
            "color": "yellow"
        },
        "PROCESSING": {
//            "label": "Processing import",
            "label": "Procesando importado",
            "color": "yellow"
        },
        "COMPLETE": {
//            "label": "Finished import",
            "label": "Terminó el importado",
            "color": "green"
        },
        "FAILED": {
//            "label": "Import failed for one or more uploads",
            "label": "El importado falló para una o más cargas",
            "color": "red"
        }
    },
    "scene": {
        "NOTINGESTED": {
//            "label": "Not scheduled for ingest",
            "label": "No programado para la ingesta",
            "color": "grey"
        },
        "TOBEINGESTED": {
//            "label": "Queued for ingest",
            "label": "En cola para la ingesta",
            "color": "yellow"
        },
        "INGESTING": {
//            "label": "Being ingested",
            "label": "Siendo ingestado",
            "color": "yellow"
        },
        "INGESTED": {
//            "label": "Successfully ingested",
            "label": "Ingestado",
            "color": "green"
        },
        "COG": {
//            "label": "Cloud Optimized",
            "label": "Optimizado para la nube",
            "color": "blue"
        },
        "FAILED": {
//            "label": "Ingest failed",
            "label": "Falló la ingesta",
            "color": "red"
        }
    },
    "project": {
        "NOSCENES": {
//            "label": "No scenes",
            "label": "No hay escenas",
            "color": "grey"
        },
        "LARGE": {
//            "label": "Too many scenes to preview",
            "label": "Demasiadas escenas para la vista previa",
            "color": "grey"
        },
        "PARTIAL": {
//            "label": "Not all scenes have been ingested",
            "label": "No todas las escenas han sido ingestadas",
            "color": "yellow"
        },
        "PENDINGREVIEW": {
//            "label": "Scenes are pending review",
            "label": "Las escenas están esperando revisión",
            "color": "yellow"
        },
        "CURRENT": {
//            "label": "All scenes ingested",
            "label": "Toas las escenas ingestadas",
            "color": "green"
        },
        "FAILED": {
//            "label": "Ingest failed for some scenes",
            "label": "La ingesta falló para algunas imágenes",
            "color": "red"
        }
    }
};

export default (app) => {
    class StatusService {
        constructor() {
            'ngInject';
        }

        getStatusFields(entityType, status) {
            return _.get(statusMapping, [entityType, status]) || false;
        }
    }

    app.service('statusService', StatusService);
};
